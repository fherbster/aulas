#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/stat.h>

#define TRUE 1

int main() {
    char command[100];
    char parameters[10][100]; // Matriz de caracteres para armazenar os parâmetros
    char *args[12]; // Array de ponteiros de caracteres para passar para execvp()
    int status;

    while (TRUE) {
        printf("shell> ");
        fflush(stdout); // Limpar o buffer de saída para garantir que a mensagem seja exibida imediatamente

        // Ler a linha de entrada do usuário
        char input[200];
        fgets(input, sizeof(input), stdin);

        // Remover o caractere de quebra de linha da entrada
        input[strcspn(input, "\n")] = '\0';

        // Analisar a linha de entrada para extrair o comando e os parâmetros
        sscanf(input, "%s", command);
        int num_params = 0;
        char *token = strtok(input, " ");
        while (token != NULL && num_params < 10) {
            strcpy(parameters[num_params++], token);
            token = strtok(NULL, " ");
        }

        // Verificar se o comando é "cd"
        if (strcmp(command, "cd") == 0) {
            if (num_params > 2) {
                // Se sim e se houver mais de um parâmetro, reportar erro
                fprintf(stderr, "cd: too many arguments\n");
            } else if (num_params == 2) {
                // Se sim e se houver exatamente um parâmetro, tentar mudar para o diretório especificado
                struct stat st;
                if (stat(parameters[1], &st) == 0 && S_ISDIR(st.st_mode)) {
                    // Se o diretório existir, chamar a função chdir()
                    if (chdir(parameters[1]) != 0) {
                        perror("cd");
                    }
                } else {
                    fprintf(stderr, "cd: No such directory: %s\n", parameters[1]);
                }
            } else {
                // Se não houver parâmetros suficientes, mudar para o diretório home do usuário
                if (chdir(getenv("HOME")) != 0) {
                    perror("cd");
                }
            }
            continue; // Ir para a próxima iteração do loop
        }

        // Verificar se o comando é "ls"
        if (strcmp(command, "ls") == 0) {
            // Utilizar o comando ls do sistema
            system("ls");
            continue; // Ir para a próxima iteração do loop
        }

        // Construir o array de argumentos para execvp()
        args[0] = command;
        for (int i = 0; i < num_params; i++) {
            args[i + 1] = parameters[i];
        }
        args[num_params + 1] = NULL; // Sinalizar o fim dos argumentos

        // Criar um processo filho
        pid_t pid = fork();
        if (pid == -1) {
            perror("fork");
            exit(EXIT_FAILURE);
        }

        if (pid == 0) {
            // Processo filho
            execvp(command, args); // Executar o comando
            // Se execvp retornar, houve um erro
            fprintf(stderr, "Erro ao executar o comando.\n");
            exit(EXIT_FAILURE);
        } else {
            // Processo pai
            waitpid(pid, &status, 0); // Esperar pelo processo filho
        }
    }

    return 0;
}
