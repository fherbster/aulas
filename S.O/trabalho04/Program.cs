using System;
using System.Collections.Generic;

class INode
{
    public string Name { get; set; }
    public int Size { get; set; }
    public List<string> DataBlocks { get; set; }

    public INode(string name, int size)
    {
        Name = name;
        Size = size;
        DataBlocks = new List<string>();
    }
}

class Directory : INode
{
    public Directory Parent { get; set; }
    public List<INode> Children { get; set; }

    public Directory(string name, Directory parent = null) : base(name, 0)
    {
        Parent = parent;
        Children = new List<INode>();
    }

    public void ListFiles()
    {
        Console.WriteLine($"Conteúdo do diretório: {Name}");
        foreach (var child in Children)
        {
            Console.WriteLine(child.Name);
        }
    }

    public Directory Navigate(string name)
    {
        if (name == ".." && Parent != null)
        {
            return Parent;
        }
        else if (name == ".")
        {
            return this;
        }
        else
        {
            foreach (var child in Children)
            {
                if (child is Directory dir && dir.Name == name)
                {
                    return dir;
                }
            }
        }
        return null;
    }
}

class File : INode
{
    public File(string name, int size) : base(name, size) { }

    public void Write(string data)
    {
        DataBlocks.Add(data);
        Size += data.Length;
    }

    public void Read()
    {
        foreach (var block in DataBlocks)
        {
            Console.WriteLine(block);
        }
    }
}

class FileSystem
{
    public Directory Root { get; set; }
    public Directory CurrentDirectory { get; set; }

    public FileSystem()
    {
        Root = new Directory("root");
        CurrentDirectory = Root;
    }

    public void CreateFile(string name, int size)
    {
        File newFile = new File(name, size);
        CurrentDirectory.Children.Add(newFile);
    }

    public void CreateDirectory(string name)
    {
        Directory newDir = new Directory(name, CurrentDirectory);
        CurrentDirectory.Children.Add(newDir);
    }

    public void MoveFile(string name, Directory targetDir)
    {
        File fileToMove = null;

        // Verifica se o arquivo existe no diretório atual
        foreach (var child in CurrentDirectory.Children)
        {
            if (child is File file && file.Name == name)
            {
                fileToMove = file;
                break;
            }
        }

        if (fileToMove != null)
        {
            // Remove o arquivo do diretório atual e adiciona ao diretório de destino
            CurrentDirectory.Children.Remove(fileToMove);
            targetDir.Children.Add(fileToMove);
            Console.WriteLine($"Arquivo '{name}' movido com sucesso.");
        }
        else
        {
            Console.WriteLine($"Erro: Arquivo '{name}' não encontrado no diretório atual.");
        }
    }


    public void DeleteFile(string name)
    {
        INode fileToDelete = null;
        foreach (var child in CurrentDirectory.Children)
        {
            if (child.Name == name)
            {
                fileToDelete = child;
                break;
            }
        }

        if (fileToDelete != null)
        {
            CurrentDirectory.Children.Remove(fileToDelete);
        }
    }

    public void ChangeDirectory(string name)
    {
        Directory targetDir = CurrentDirectory.Navigate(name);
        if (targetDir != null)
        {
            CurrentDirectory = targetDir;
        }
        else
        {
            Console.WriteLine("Diretório não encontrado.");
        }
    }

    public string GetCurrentPath()
    {
        Stack<string> pathStack = new Stack<string>();
        Directory tempDir = CurrentDirectory;

        while (tempDir != null)
        {
            pathStack.Push(tempDir.Name);
            tempDir = tempDir.Parent;
        }

        return "/" + string.Join("/", pathStack);
    }
}

class Program
{
    static void Main(string[] args)
    {
        FileSystem fs = new FileSystem();

        while (true)
        {
            Console.WriteLine("\n--- Sistema de Arquivos ---");
            Console.WriteLine("Diretório atual: " + fs.GetCurrentPath());
            Console.WriteLine("1. Criar Arquivo");
            Console.WriteLine("2. Criar Diretório");
            Console.WriteLine("3. Mover Arquivo");
            Console.WriteLine("4. Deletar Arquivo");
            Console.WriteLine("5. Listar Arquivos");
            Console.WriteLine("6. Mudar de Diretório");
            Console.WriteLine("7. Escrever em Arquivo");
            Console.WriteLine("8. Ler Arquivo");
            Console.WriteLine("9. Mostrar Caminho Atual");
            Console.WriteLine("10. Sair");
            Console.Write("Escolha uma opção: ");
            int opcao = int.Parse(Console.ReadLine());

            switch (opcao)
            {
                case 1:
                    Console.Write("Nome do arquivo: ");
                    string fileName = Console.ReadLine();
                    Console.Write("Tamanho do arquivo: ");
                    int fileSize = int.Parse(Console.ReadLine());
                    fs.CreateFile(fileName, fileSize);
                    break;

                case 2:
                    Console.Write("Nome do diretório: ");
                    string dirName = Console.ReadLine();
                    fs.CreateDirectory(dirName);
                    break;

                case 3:
                    Console.Write("Nome do arquivo a ser movido: ");
                    string moveFileName = Console.ReadLine();
                    Console.Write("Nome do diretório de destino: ");
                    string moveDirName = Console.ReadLine();

                    // Verifica se o diretório de destino existe
                    Directory targetDir = fs.CurrentDirectory.Navigate(moveDirName);
                    if (targetDir != null)
                    {
                        // Diretório de destino encontrado, tenta mover o arquivo
                        fs.MoveFile(moveFileName, targetDir);
                        Console.WriteLine($"Arquivo '{moveFileName}' movido para o diretório '{moveDirName}'.");
                    }
                    else
                    {
                        Console.WriteLine("Erro: Diretório de destino não encontrado.");
                    }
                    break;

                case 4:
                    Console.Write("Nome do arquivo a ser deletado: ");
                    string delFileName = Console.ReadLine();
                    fs.DeleteFile(delFileName);
                    break;

                case 5:
                    fs.CurrentDirectory.ListFiles();
                    break;

                case 6:
                    Console.Write("Digite o nome do diretório (.. para voltar): ");
                    string cdName = Console.ReadLine();
                    fs.ChangeDirectory(cdName);
                    break;

                case 7:
                    Console.Write("Nome do arquivo para escrever: ");
                    string writeFile = Console.ReadLine();
                    foreach (var child in fs.CurrentDirectory.Children)
                    {
                        if (child is File file && file.Name == writeFile)
                        {
                            Console.Write("Conteúdo a ser escrito: ");
                            string data = Console.ReadLine();
                            file.Write(data);
                            break;
                        }
                    }
                    break;

                case 8:
                    Console.Write("Nome do arquivo para ler: ");
                    string readFile = Console.ReadLine();
                    foreach (var child in fs.CurrentDirectory.Children)
                    {
                        if (child is File file && file.Name == readFile)
                        {
                            file.Read();
                            break;
                        }
                    }
                    break;

                case 9:
                    Console.WriteLine("Caminho Atual: " + fs.GetCurrentPath());
                    break;

                case 10:
                    return;

                default:
                    Console.WriteLine("Opção inválida.");
                    break;
            }
        }
    }
}
