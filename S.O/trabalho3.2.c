#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MAX_PAGES 1000
#define MAX_FRAMES 100

void generate_page_references(int references[], int num_references, int num_pages) {
    for (int i = 0; i < num_references; i++) {
        references[i] = rand() % num_pages;
    }
}

int fifo(int references[], int num_references, int num_frames) {
    int frames[MAX_FRAMES], next_frame = 0, page_faults = 0;
    int frame_status[MAX_FRAMES] = {0}; 

    for (int i = 0; i < num_references; i++) {
        int page = references[i];
        int found = 0;
        
        for (int j = 0; j < num_frames; j++) {
            if (frame_status[j] == 1 && frames[j] == page) {
                found = 1;
                break;
            }
        }

        if (!found) {
            frames[next_frame] = page;
            frame_status[next_frame] = 1;
            next_frame = (next_frame + 1) % num_frames;
            page_faults++;
        }
    }
    return page_faults;
}

int aging(int references[], int num_references, int num_frames) {
    int frames[MAX_FRAMES], page_faults = 0;
    unsigned int ages[MAX_FRAMES] = {0};

    for (int i = 0; i < num_references; i++) {
        int page = references[i];
        int found = 0, frame_to_replace = 0;

        for (int j = 0; j < num_frames; j++) {
            ages[j] >>= 1;
        }

        for (int j = 0; j < num_frames; j++) {
            if (frames[j] == page) {
                found = 1;
                ages[j] |= (1 << 31);
                break;
            }
        }

        if (!found) {
            unsigned int min_age = 0xFFFFFFFF;

            for (int j = 0; j < num_frames; j++) {
                if (ages[j] < min_age) {
                    min_age = ages[j];
                    frame_to_replace = j;
                }
            }

            frames[frame_to_replace] = page;
            ages[frame_to_replace] = (1 << 31);
            page_faults++;
        }
    }
    return page_faults;
}

int main() {
    int num_frames, num_references = MAX_PAGES;
    int references[MAX_PAGES];

    srand(time(NULL));

    generate_page_references(references, num_references, MAX_FRAMES);

    FILE *output = fopen("page_faults.txt", "w");
    if (output == NULL) {
        printf("Erro ao abrir o arquivo!\n");
        return 1;
    }

    for (num_frames = 1; num_frames <= MAX_FRAMES; num_frames++) {
        int fifo_faults = fifo(references, num_references, num_frames);
        int aging_faults = aging(references, num_references, num_frames);

        fprintf(output, "Molduras: %d, FIFO: %d, Envelhecimento: %d\n", num_frames, fifo_faults, aging_faults);
        printf("Molduras: %d, FIFO: %d, Envelhecimento: %d\n", num_frames, fifo_faults, aging_faults);
    }

    fclose(output);
    return 0;
}
